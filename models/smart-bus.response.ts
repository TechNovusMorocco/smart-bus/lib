
export interface SmartBusResponse<T> {
    status: STATUS
    message: string | Object | unknown
    data?: T 
}
/**
 * the status of a response .
 * values :
 * 
 * - -2 : internal server 
 */
export enum STATUS {
    SUCCESS = 1,
    LOGIC_ERROR = 0,
    BAD_REQUEST = -1,
    SERVER_ERROR = -2,
    WS_ERROR =-3
}
/**
 * la reponse standar de Smart Bus BackEnd  .
 * @param {STATUS} status   le status de la reponse
 * @param {string, Object }  message  le message 
 * @param {any}  data   data envoyer (objet ,array ...)
 */
export function getResponse<T>(status: STATUS, message: string | Object | unknown, data?: T) {
    let resp: SmartBusResponse<T> = { status, message, data }
    return resp
}
