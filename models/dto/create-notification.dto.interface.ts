import { SENDER_TYPE } from '@lib/models/interfaces/notificationRecord.interface';


export interface CreateNotificationDto{
    tuteurIds : number[]


    message : string 

    heading:string


    sender:SENDER_TYPE


    ecoleId: number


    messageType:NOTIFICATION_TYPE

}

export enum NOTIFICATION_TYPE{
    MESSAGE=0,
    INFORMATION=1
}