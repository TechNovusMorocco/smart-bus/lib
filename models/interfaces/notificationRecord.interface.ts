import { Ecole } from './ecole.interface';
import { file } from './file.interface';
import { NotificationRecordItem } from './notificationRecordItem.interface';
import { notificationStatus } from './notificationStatus.interface';



export interface notificationRecord {

    id : number
    idNotification : string
    creationDate : Date
    sentDate : Date
    recipients : number // number of successful notifications sent
    totalSent : number //to compare recipients with the total of notif that was sent
    message : string 
    subtitle : string
    type : number; // 0=informative notification & 1= notification message
    files : file[]
    sender  : SENDER_TYPE  //school or automatic 
    ecole : Ecole //many to one
    received : number 
    seen : number
    notificationStatus : notificationStatus[] //One to many 
    notificationRecordItems : NotificationRecordItem[]

}

export enum SENDER_TYPE {
    ECOLE = 'ECOLE',
    AUTOMATIQUE = 'AUTOMATIQUE'
}