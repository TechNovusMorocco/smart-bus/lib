import { createAdresseDtoInterface } from "./create-adresse.dto.interface";

export interface CreateEcoleDtoInterface {
    
    nomEcole: string;
    email: string;
    userName: string;
    password: string;
    tel: string;
    adresse:createAdresseDtoInterface;

   


}
