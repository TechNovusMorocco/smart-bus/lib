import { Message } from '@lib/models/interfaces/message.interface';
import { Tuteur } from '@lib/models/interfaces/tuteur.inteface';

export class MessageDetailsDto {

    etat : number
    messageId : Message
    tuteurId : Tuteur

}