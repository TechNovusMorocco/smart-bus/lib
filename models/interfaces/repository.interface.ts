export interface Repository{
    /**
     * la creation d'une entitie 
     */
    create(...createdDto:any)
    /**
     * la modification d'une entitie 
     */
    edit(...editDto:any)
    /**
     * la supprission  d'une entitie 
     */
    delete(...deleteDto:any)
}