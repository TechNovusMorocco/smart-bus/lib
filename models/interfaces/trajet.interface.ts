import { Adresse } from "./adresse.interface";
import { Transport } from "./transport.interface";
import { TrajetEtape } from './trajet-etape.interface';

export interface Trajet{
    id:number
    adresse:Adresse
    transport:Transport
    /**
     * le numéro du trajet 
     */
    numero:number
    /**
     * etat du trajet .
     * 
     * values
     * - -2 supprimer 
     * - 0 aller 
     * - 1 retour 
     */
    etat:number
    /**
     * l'état de commencement du trajet .
     * 
     * values :
     * - 0  le transport n'a pas commencer le trajet 
     * - 1  le transport a commencer le trajet 
     */
    live:number
    trajetEtapes:TrajetEtape[]

}