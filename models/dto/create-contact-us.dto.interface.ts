export interface CreateContactUsDtoInterfacace{
    nom: string;
    email:string
    tel: string;
    message: string;
    type:CONTACT_US_TYPE
}
export enum CONTACT_US_TYPE{
    ECOLE="ECOLE",
    TUTEUR="TUTEUR",
    TUTEUR_FROM_APP="TUTEUR_FROM_APP",
    MANAGER_FROM_WEB_APP="MANAGER_FROM_WEB_APP"
}