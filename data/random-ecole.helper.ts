import * as faker from "faker"
import { RandomAdresseHelper } from './random-adresse.helper'
import { Ecole } from "@lib/models/interfaces/ecole.interface"
 
export class RandomEcolehelper {

    static getEcoleByNbr(nbr: number) {
        let ecoles: Ecole[] = []
        for (let index = 0; index < nbr; index++) {
            let ecole = {
                adresse: RandomAdresseHelper.getAdressByNbr(1)[0],
                email: faker.internet.email(),
                etat: -1,
                id: faker.random.number(),
                nomEcole: "school " + faker.company.companyName(),
                password: faker.internet.password(),
                tel: faker.phone.phoneNumber("+212"),
                userName: faker.internet.userName()
            }
            ecoles.push(ecole)
        }
        return ecoles
    }
}