import { Tuteur } from "./tuteur.inteface";
import { Device } from "./device.interface";


export interface NotificationRecordItem {

    id : number 
    tuteur : Tuteur
    seenDate : Date
    ReceivedDate : Date
    devices : Device[]

}