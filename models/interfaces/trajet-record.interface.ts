import { TrajetEtape } from "./trajet-etape.interface";
import { TrajetRecordItem } from "./trajet-record-item.interface";
import { Trajet } from "./trajet.interface";

export interface TrajetRecord{
    id?:number
    /**
     * date de debut du trajet
     */
    debutTrajet:number
    /**
     * date de fin du trajet
     */
    finTrajet:number
    polylign:string
    nbrExcesVitesse:number
    /**
     * une liste des exces de vitesse se form polylign(string) separer par ,
     */
    excesPolylign:string

    /**
     * le trajet concerner .
     */
    trajet:Trajet

    /**
     * la liste choisi par le conducetur 
     */
    trajetEtapes?:TrajetEtape[]
    /**
     * trajet record item 
     */
    trajetRecordItems:TrajetRecordItem[]
}