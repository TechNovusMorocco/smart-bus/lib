import { Ecole } from './ecole.interface';
import { AccessUser } from './access-user';
import { MessageDetaille } from './message-detaille.interface';
import { file } from './file.interface';
export interface Message {
    id: number
    obj: string
    contenu: string
    dateMessage: Date
    ecole : Ecole
    userAccess:AccessUser
    /**
     * l'éxpiditeur du message (école ou bien transport)
     * @type {ENVOYER_PAR}  ECOLE , TRANSPORT
     */
    envoyerPar: ENVOYER_PAR
    /**
     * piece joint 
     */
    pj: boolean

    messageDetails?:MessageDetaille[]
    files?:file[]
    
}

export enum ENVOYER_PAR {
    ECOLE = "envoyer par l'école",
    TRANSPORT = "envoyer par le transport",
    MANAGER="envoyer par manager"
}
