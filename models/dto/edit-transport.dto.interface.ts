export interface EditTransportDtoInterface {
    transportId: number
    conducteur:
    {
        nom: string
        prenom: string
        tel: string
    }
    simNumber: string
    capacite: number
}