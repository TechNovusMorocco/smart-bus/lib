import { CreateClasseDtoInterface } from "./create-classe.dto.interface";
import { createAdresseDtoInterface } from "./create-adresse.dto.interface";

export class CreateEtudiantDtoInterface{
    nom: string;
    prenom: string;
    age: number;
    defaultAdresse: createAdresseDtoInterface;
    classe: CreateClasseDtoInterface


}