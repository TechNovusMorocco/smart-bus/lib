import { CreateEtudiantDtoInterface } from "./create-etudiant.dto.interface";
import { CreateTuteurDtoInterface } from "./create-tuteur.dto.interface";

export interface CreateEtudiantOptInterface {

    etudiants:CreateEtudiantDtoInterface[]
    tuteurs:CreateTuteurDtoInterface[]
    haseSameAdresse:boolean
  
}