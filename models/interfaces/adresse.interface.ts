export interface Adresse{
    id?:number,
    lat:number,
    lng:number,
    geoCode?:string
}

export enum MOROCCO_ADRESSE{
    LAT=33.241442,
    LNG=-7.689737
}