

export interface notificationStatus {

    id : number
    requestDate : Date //date when we send it
    receiveDate : Date // date when the user receive it
    seenDate : Date //date when the user saw it
    //notificationStatusItems : NotificationStatusItem // oneToMany
}