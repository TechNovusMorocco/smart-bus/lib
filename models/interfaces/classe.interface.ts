import { Ecole } from "./ecole.interface";

export interface Classe{
    id:number
    anneeScolaire:string
    niveau:string
    ecole:Ecole
}
