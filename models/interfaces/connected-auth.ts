import { Ecole } from './ecole.interface';
import { AccessUser } from './access-user';

export interface ConnectedAuth{
    connectedType:CONNECTED_TYPE
    ecole:Ecole
    accesUser:AccessUser

}

export enum CONNECTED_TYPE{
    /**
     * ecole that have access to all features 
     */
    ECOLE=1,
    /**
     * manager who has access to some specific feature 
     */
    MANAGER=2,
    /**
     * smart bus admin who has access to all data 
     * @description not Implimemnted yet .
     */
    ADMIN=3
}