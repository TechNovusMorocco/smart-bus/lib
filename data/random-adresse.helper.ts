import * as Faker from 'faker'
import { Adresse } from '@lib/models/interfaces/adresse.interface'
export class RandomAdresseHelper{
    
    
    static getAdressByNbr(nbr){
        let adresses:Adresse[]=[]
        for (let index = 0; index < nbr; index++) {
        let adresse : Adresse= {
            id:new Number().valueOf(),
            lat:new Number(Faker.address.latitude()).valueOf(),
            lng:new Number(Faker.address.latitude()).valueOf(),
            geoCode:Faker.address.streetName()
          }
           adresses.push(adresse) 
        }
        return adresses
    }
}