import { Etudiant } from "./etudiant.interface";
import { Trajet } from "./trajet.interface";

export interface TrajetEtape{

    id:number
    /**
     * etat du trajet etape.(l'étudiant est supprimer du trajet )
     * 
     * values :
     * - -2 l'étudiant de ce trajet etape est supprimer du trajet .
     * -  0 vient d'etre creer 
     * -  1 l'étudiant de ce trajet à changer le trajet  .
     */
    etat:number
    etudiant:Etudiant
    trajet:Trajet
    /**
     * can't be negatif 
     * 
     */
    order:number
}