import { Adresse } from "@lib/models/interfaces/adresse.interface";
import { Trajet } from "@lib/models/interfaces/trajet.interface";

export interface SetTransportLocationDto {
    trajet: Trajet
    adresse: Adresse
    client?: any

}