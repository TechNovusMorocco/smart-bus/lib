import { Message, ENVOYER_PAR } from '@lib/models/interfaces/message.interface';
import * as Faker from "faker"
import { RandomEcolehelper } from './random-ecole.helper';
import { MessageDetaille } from '@lib/models/interfaces/message-detaille.interface';
import { RandomMessageHelper } from './random-message.helper';
import { RandomTuteurHelper } from './random-tuteur.helper';
export class RandomMessageDetaill {

    static getRandomMessagByNbr(nbr: number) {
        let MessageDetaills: MessageDetaille[] = []
        for (let i = 0; i < nbr; i++) {
            let randomNumber = Faker.random.number(12)
            let etat=randomNumber <= 4 ? -1 : randomNumber <=8 ? 0 : 1
            let MessageDetaill: MessageDetaille = {
                etat,
                id: Faker.random.number(),
                message: RandomMessageHelper.getRandomMessagByNbr(1)[0],
                tuteur: RandomTuteurHelper.getTuteursByNbr(1)[0]
            }
            MessageDetaills.push(MessageDetaill)

        }
        return MessageDetaills
    }
}
