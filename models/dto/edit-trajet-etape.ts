import { TrajetEtape } from '../interfaces/trajet-etape.interface';

export interface EditTrajetEtape{
    trajetEtapeIds:number
    newOrder:number
}