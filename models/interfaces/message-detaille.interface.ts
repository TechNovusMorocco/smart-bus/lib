import { Message } from "./message.interface";
import { Tuteur } from './tuteur.inteface';

export interface MessageDetaille {
    id: number
    message: Message
    tuteur: Tuteur
    /**
     * etat du message 
     * 
     * values :
     * 
     * - -1 delivrer 
     * -  0 reçu
     * -  1 vu
     */
    etat: number
}