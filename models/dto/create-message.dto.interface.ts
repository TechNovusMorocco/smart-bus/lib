import { ENVOYER_PAR } from "../interfaces/message.interface";



export class CreateMessageDto {

    object : string;

    message : string; 

    urls : string[];

    tutorsId : number[]

    ecoleId : number

    envoyerPar:ENVOYER_PAR

    accessUserId:number

}
