import { AccessRole, ROLE_TYPE } from "../interfaces/access-role";

export interface CreateAccessUser{
    userName: string
    email: string
    password: string
    tel: string
    role: ROLE_TYPE[]
    firstName: string
    LastName: string
    classes: string[]
    ecoleId: number
}