export interface NotifiedEtudiant {

    etudiantId : number;
    tuteurIds : number[];
    trajetId : number;
    dateNotifiation? : Date;
    
}

export enum STATUS {
    APPROCHE = "approche",
    ARRIVER = "arriver"
}