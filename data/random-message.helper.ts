import { Message, ENVOYER_PAR } from '@lib/models/interfaces/message.interface';
import * as Faker from "faker"
import { RandomEcolehelper } from './random-ecole.helper';
import { RandomAccessUser } from './random-access-user.helper';
import { RandomMessageDetaill } from './random-message-detaill.helper';
import { RandomFileHelper } from './random-file.helper';
export class RandomMessageHelper {

    static getRandomMessagByNbr(nbr: number,messageDetaill?:boolean) {
        let messages: Message[] = []
        for (let i = 0; i < nbr; i++) {
            let randomNumber=Faker.random.number(15)
            let envoyerPar= randomNumber<= 5 ? ENVOYER_PAR.ECOLE :randomNumber<=10?ENVOYER_PAR.MANAGER:ENVOYER_PAR.TRANSPORT
            let message: Message = {
                contenu: Faker.lorem.paragraphs(Faker.random.number(30)),
                dateMessage: Faker.date.recent(5),
                ecole: RandomEcolehelper.getEcoleByNbr(1)[0],
                envoyerPar,
                userAccess:RandomAccessUser.getAccessUserByNbr(1)[0],
                id: Faker.random.number(),
                obj: Faker.random.words(3),
                pj: Faker.random.number(5) > 2 ? true : false,
                files:RandomFileHelper.getFilesByNbr(Faker.random.number({min:1,max:4})),
               // messageDetaill:messageDetaill?RandomMessageDetaill.getRandomMessagByNbr(Faker.random.number(20)):[]
            }
            messages.push(message)

        }
        return messages
    }
}