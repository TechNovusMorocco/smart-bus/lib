import { Adresse } from "./adresse.interface";
import { Tuteur } from "./tuteur.inteface";
import { Classe } from "./classe.interface";

export interface Etudiant{
    id?:number
    nom:string
    prenom:string
    fullName:string
    /**
     * etat de l'étudiant 
     * 
     * Values :
     *  - -2 supprimer 
     *  - -1 vient d'etre creer  (sans adresses )
     *  - 0 avec adresse mais non affecté 
     *  - 1 affecté à un seul trajet 
     *  - 1+ affecté à plusieur trajets 
     */
    etat:number 
    age:number
    createdAt:number
    /**
     * état des adressses de l'étudiant.
     * 
     * Vaalues  :
     *  - 1 l'étudiant déspose d'une adresse par defaut
     *  - 2 l'étdudiant despose d'une deuxieme adresse
     *  - 3 l'étudiant despose d'une troisiem adresse
     */
    etatAdress:number
    /**
     * perimetre de notification .
     * 
     */
    perimetre:number
    /**
     * le nombre des notificatios 
     */
    nbrNotification:number

    hasBrother:boolean
    defaultAdresse:Adresse
    deuxiemAdresse:Adresse
    troisiemAdresse:Adresse
    tuteurs:Tuteur[]
    classe:Classe


}



