export const markersIcon ={
    ecole:{
        url: "assets/markers/schoolMarker.png", scaledSize: {
          width: 55,
          height: 55
        }
      },
    transport:{
        url: "assets/markers/busmarker.png", scaledSize: {
          width: 50,
          height: 50 
        }
      },
    etudiant:{
        url: "assets/markers/etudiantmarker.png", scaledSize: {
          width: 50,
          height: 50
        }
      },
      etudiantWithNoBus:{
        url: "assets/markers/etudiantmarker-with-No-Bus.png", scaledSize: {
          width: 50,
          height: 50
        }
      }
}