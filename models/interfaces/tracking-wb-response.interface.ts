import { Adresse } from "./adresse.interface";
import { Trajet } from "./trajet.interface";

export interface TrackingWbResponse{
    
    /**
     * 
     */
    polylign:string
    lastAdresse:Adresse
    trajet:Trajet
    requestedBy:RequestedBy


}

export enum RequestedBy {
    ecole="ECOLE",
    tuteur="TUTEUR",

}