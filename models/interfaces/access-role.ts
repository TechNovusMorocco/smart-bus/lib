export interface AccessRole{
    id:number
    type:ROLE_TYPE
    

}

export enum ROLE_TYPE{
    CREATE_MESSAGE=1,
    MANAGE_ETUDIANT=2,
    MANAGE_TRAJETS=3
}