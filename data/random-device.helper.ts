import * as faker from "faker"
import { Device, DEVICE_TYPE } from "@lib/models/interfaces/device.interface"
export class RandomDeviceHelper{
    
    static getRandomDeviceByNbr(nbr:number){
        let devices:Device[]=[]

        for (let index = 0; index < nbr; index++) {
            let device :Device ={
                id:faker.random.number(),
                connctedAt:faker.date.past().getTime(),
                connected:faker.random.number({min:0,max:3})>1?true:false,
                deviceId:faker.random.uuid(),
                disconnctedAt:faker.date.past().getTime(),
                type:faker.random.number({min:0,max:3})>1?DEVICE_TYPE.ANDROID:DEVICE_TYPE.IOS
            }
            devices.push(device)
        }
        return devices
    }
}