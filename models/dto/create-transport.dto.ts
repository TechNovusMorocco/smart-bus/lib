import { CreateConducteurInterface } from "./create-conducteur.dto";
import { createAccompagnatriceDtoInterface } from "./create-accompagnatrice.dto";

export interface createTransportDtoInterface {

    mattricule: string;
    capacite: number;
    simNumber:string;
    conducteur:CreateConducteurInterface
    accompagnatrice: createAccompagnatriceDtoInterface
    createdAt: number;


}