
import { SENDER_TYPE } from '@lib/models/interfaces/notificationRecord.interface';


export class CreateNotificationPicDto{
    
    tuteurIds : number[]

   

    message : string 

  

    sender:SENDER_TYPE

   

    ecoleId: number

   
    messageType:NOTIFICATION_TYPE

    
    imageType :IMAGE_TYPE

  
    image : string

}

export enum NOTIFICATION_TYPE{
    MESSAGE=0,
    INFORMATION=1
}

export enum IMAGE_TYPE{
    JPEG = "JPEG",
    PNG = "PNG",
    EXCEL = "EXCEL",
    PDF= "PDF",
}