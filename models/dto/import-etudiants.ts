export interface ImportEtudiants {
    createImportedEtudiant:CreateImportedEtudiant[]
    createImportedTuteur:CreateImportedTuteur[]
    hasbrothers:boolean
    hasTuteurs:boolean
    possibleBrothersIndex?:number[]
    possibleTuteursIndex?:number[]
    ecoleId?:number

}

export interface CreateImportedEtudiant {
    nom: string
    prenom: string
    classe: string
}
export interface CreateImportedTuteur {
    tel: string
}

