export interface EditEtudiant {
  id? : number;
  nom : string;
  prenom: string;
  class? : string;
  lat? : number;
  lng? : number;
  tel?:string;
  //si il est tuteur ou bien etudiant
  tuteurs?:boolean;
}
