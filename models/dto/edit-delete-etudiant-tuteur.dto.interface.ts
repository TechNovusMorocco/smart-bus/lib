import { EditEtudiant } from "./edit-etudiant.dto.interface";

export interface EditDeleteEtudiantTuteur {
  etudiantToEdits : EditEtudiant[] ;
  etudianttsTodeletes : EditEtudiant[];
  etudiantsToCreate : EditEtudiant[];
  tuteur : EditEtudiant;

}
