import { Trajet } from '@lib/models/interfaces/trajet.interface';
import { Adresse } from '@lib/models/interfaces/adresse.interface';
import { TrajetEtape } from '@lib/models/interfaces/trajet-etape.interface';
import { Etudiant } from '../interfaces/etudiant.interface';

export interface TrsRealTimeLocationDto{
    trajet:Trajet
    adresses:Adresse[]
    vitesse:number
    currentAdresse:Adresse
    polylign:string
    outOfBusTrjEtp:TrajetEtape[]
    valider:TrajetEtape[]
    absent:TrajetEtape[]
    changeType:CHANGE_TYPE
    etudiantTrajet ?: Etudiant[]

}

export enum CHANGE_TYPE{
    NEW_LOCATION="NEW_LOCATION_FOUND",
    NEW_ABSENT="NEW_ABSENT_TRJ_ETP",
    NEW_VALIDER="NEW_VALID_TRJ_ETP",
    JUST_STARTING="JUST_STARTING_TRAJET",
    TRAJET_FINISH="TRAJET_FINISH"
}