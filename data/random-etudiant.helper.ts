import { Etudiant } from '@lib/models/interfaces/etudiant.interface';
import * as faker from 'faker'
import { RandomEcolehelper } from './random-ecole.helper';
import { RandomAdresseHelper } from './random-adresse.helper';
import { RandomTuteurHelper } from './random-tuteur.helper';
export class RandomEtudiantHelper{


    static getEtudiantByNbr(nbr:number){
        let etudiants:Etudiant[]=[]
        for (let index = 0; index < nbr; index++) {
            let etudiant:Etudiant={
                hasBrother:faker.random.number({min:0,max:10})>5,
                 age:faker.random.number({min:10,max:18}),
                 classe:{
                     anneeScolaire:""+faker.date.past().getFullYear()+"/"+faker.date.past().getFullYear(),
                    ecole:RandomEcolehelper.getEcoleByNbr(1)[0],
                     id:faker.random.number(),
                     niveau:faker.random.word(),

                 },
                 defaultAdresse:RandomAdresseHelper.getAdressByNbr(1)[0],
                 troisiemAdresse:RandomAdresseHelper.getAdressByNbr(1)[0],
                 deuxiemAdresse:RandomAdresseHelper.getAdressByNbr(1)[0],
                 createdAt:faker.date.past().getTime(),
                 etat:faker.random.number({min:-1}),
                 etatAdress:faker.random.number({min:1,max:3}),
                 fullName:faker.name.firstName(),
                 nom:faker.name.firstName(),
                 prenom:faker.name.firstName(),
                 nbrNotification:faker.random.number({min:1,max:5}),
                 perimetre:faker.random.number(),
                 id:faker.random.number(),
                 tuteurs:RandomTuteurHelper.getTuteursByNbr(Math.round(Math.random()*10))

            }
            etudiants.push(etudiant)
            
        }
        return etudiants
    }
}