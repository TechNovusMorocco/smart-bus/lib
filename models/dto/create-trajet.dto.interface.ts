import { CreateTrajetEtapeDtoInterface } from "./create-trajet-etape.dto.interface";

export interface CreateTrajetDtoInterface{
   
      /**
     * etat du trajet .
     * 
     * values
     * - -2 supprimer 
     * - 0 aller 
     * - 1 retour 
     */
    etat:number ;
    /**
     * le numéro du trajet 
     */
    numero:number
    idConducteur:number
    trajetEtapes:CreateTrajetEtapeDtoInterface[]
}