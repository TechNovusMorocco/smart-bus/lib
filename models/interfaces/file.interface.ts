

export interface file {

    id : number
    url : string
    type : FILETYPE
    creationDate : Date
}


export enum FILETYPE{
    EXCEL = 'EXCEL', 
    JPEG = "JPEG",
    PNG = "PNG",
    PDF = 'PDF',
    JPG = 'JPG'
}