import { FILETYPE } from "../interfaces/file.interface"




export class CreateFileDto {
    url : string
    type : FILETYPE
    creationDate : Date
    messageId : number
}