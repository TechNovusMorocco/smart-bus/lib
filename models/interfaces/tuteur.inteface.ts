import { Device } from "./device.interface";
import { Etudiant } from './etudiant.interface';

export interface Tuteur {

    id: number
    tel:string
    email:string
    userName:string
    nom:string
    prenom:string
    /**
     * etate of an tuteur paid / supprimer / vient d'etre creer 
     * - -2 supprimer 
     * - -1 vient d'értre creer  
     * - 0 didn't pay 
     * - 1 all good 
     */
    etat:number
    devices:Device[]
    etudiants?:Etudiant[]
}