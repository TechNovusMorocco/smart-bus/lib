export interface Device {
    id: number
    /**
     * one signale play id 
     */
    deviceId: string
    /**
    * le type du device connecter .
    * @enum {DEVICE_TYPE} ANDROID or IOS
    */
    type: DEVICE_TYPE
    /**
     * la date du 1ére connexion .(timestamp)
     */
    connctedAt:number
    /**
     * la date de la deconnexion .(timestamp)
     */
    disconnctedAt:number
    /**
     * état de connexion (soit connecter true ou bien deconnecter false )
     */
    connected:boolean
    

}

export enum DEVICE_TYPE {
    ANDROID = 'ANDROID',
    IOS = "IOS",

}
