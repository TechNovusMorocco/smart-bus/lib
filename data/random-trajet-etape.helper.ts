import { TrajetEtape } from '@lib/models/interfaces/trajet-etape.interface';
import * as faker from "faker";
import { RandomEtudiantHelper } from './random-etudiant.helper';
export class  RandomTrajetEtapeHelper{


    static getTrajetEtapesByNbr(nbr:number){
        let trajetEtapes:TrajetEtape[]=[]
        for (let index = 0; index < nbr; index++) {
            let trajetEtp:TrajetEtape ={
                etat:0,
                etudiant:RandomEtudiantHelper.getEtudiantByNbr(1)[0],
                 order:faker.random.number({min:1,max:35}),
                id:faker.random.number(),
                trajet:null
            }
            trajetEtapes.push(trajetEtp)
        }
        return trajetEtapes
    }
}