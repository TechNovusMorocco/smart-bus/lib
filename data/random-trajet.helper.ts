import { Trajet } from '@lib/models/interfaces/trajet.interface'
import * as Faker from 'faker'
import { RandomTransportHelper } from './random-transport.helper'
import { RandomTrajetEtapeHelper } from './random-trajet-etape.helper'
import { RandomAdresseHelper } from './random-adresse.helper'

export class RandomTrajetHelper{

    public static getTrajetsbynbr(nbr:number):Trajet[]{
        let trajets:Trajet[]=[]
        
        for(let i=0;i<nbr;i++){
          let trj = 
            trajets.push(
              {
                etat:Faker.random.number(1),
                id:Faker.random.number(),
                live:1,
                numero:Faker.random.number(),
                adresse:RandomAdresseHelper.getAdressByNbr(1)[0],
                transport:RandomTransportHelper.getTrsbyNbr(1)[0],
                trajetEtapes:RandomTrajetEtapeHelper.getTrajetEtapesByNbr(5)
              }
              
            )
          }
        return trajets
    }
    
}