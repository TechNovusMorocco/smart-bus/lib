import { AccessUser } from '@lib/models/interfaces/access-user';
import * as Faker from "faker"
import { RandomEcolehelper } from './random-ecole.helper';
import { AccessRole, ROLE_TYPE } from '@lib/models/interfaces/access-role';
export class RandomAccessUser {


    static getAccessUserByNbr(nbr) {
        let accessUsers: AccessUser[] = []
        let classes = this.getRandomClasses()
        for (let i = 0; i < nbr; i++) {
            let accessUser: AccessUser = {
                LastName: Faker.name.lastName(),
                firstName: Faker.name.firstName(),
                classes,
                ecole:RandomEcolehelper.getEcoleByNbr(1)[0],
                email:Faker.internet.email(),
                id:Math.round(Faker.random.number(100)),
                password:Faker.internet.password(),
                roles:this.getRandomRoles(),
                tel:Faker.phone.phoneNumber(),
                userClasses:classes.join(","),
                userName:Faker.internet.userName()
            }
            accessUsers.push(accessUser)
        }

        return accessUsers
    }

    private static getRandomClasses() {
        let size = Math.round(Faker.random.number(5))
        let classes: string[] = []
        for (let i = 0; i < size; i++) {
            classes.push(Faker.name.title())
        }
        return classes
    }
    private static getRandomRoles(){
        let size = Math.round(Faker.random.number(5))
        let roles: AccessRole[] = []
        for (let i = 0; i < size; i++) {
            let accesRole:AccessRole={
                 id:Faker.random.number(100),
                type: Faker.random.number(5)>2? ROLE_TYPE.CREATE_MESSAGE:ROLE_TYPE.MANAGE_ETUDIANT
            }
            roles.push(accesRole)
        }
        return roles
    }
}