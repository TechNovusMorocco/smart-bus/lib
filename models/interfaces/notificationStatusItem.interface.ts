import { Device } from './device.interface';
import { Tuteur } from './tuteur.inteface';


export interface NotificationStatusItem {

    id: number
    receivedDate : Date
    seenDate : Date
    tuteur :  Tuteur[]
    device : Device
}