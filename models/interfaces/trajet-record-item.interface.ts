import { TrajetEtape } from "./trajet-etape.interface";

export interface TrajetRecordItem {
    id: number
    /**
     * debut est la date en timestamp de reception du 1ere notification
     */
    debut: number
    /**
    * fin est la date en timestamp recuperation ou bien desposition de l'étudiant
    */
    fin: number
    /**
     * la différence entre fin et debut (fin-debut)
     */
    delay: number
    /**
     * le type de changement 
     */
    type:TRAJET_RECORD_TYPE
    trajetEtape:TrajetEtape
}

export enum TRAJET_RECORD_TYPE{
    RECUPERER=0,
    DEPOSER=1,
    ABSENT=-1
}