export interface Conducteur{
    id:number
    nom:string
    prenom:string
    tel:string
    fullName:string
}