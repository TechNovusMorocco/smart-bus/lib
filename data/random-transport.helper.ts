
import { Transport } from '@lib/models/interfaces/transport.interface'
import * as faker from "faker"

export class RandomTransportHelper{

    public static getTrsbyNbr(nbr:number):Transport[]{
        let transports:Transport[]=[]
        for (let index = 0; index < nbr; index++) {
            let transport:Transport ={
                    etat:-1,
                    simNumber:faker.random.number(9).toString(),
                    accompagnatrice:{
                        id:faker.random.number(),
                        fullName:faker.name.firstName()+" "+faker.name.lastName(),
                        nom:faker.name.lastName(),
                        prenom:faker.name.firstName(),
                        tel:faker.phone.phoneNumber("+212")
    
                    },
                    capacite:faker.random.number(35),
                    conducteur:{
                        id:faker.random.number(),
                        fullName:faker.name.firstName()+" "+faker.name.lastName(),
                        nom:faker.name.lastName(),
                        prenom:faker.name.firstName(),
                        tel:faker.phone.phoneNumber("+212")
    
                    },
                    createdAt:faker.date.past().getTime(),
                    ecole:null,
                    id:faker.random.number(),
                    mattricule:faker.random.words(6)
    
            }
            transports.push(transport)
            
        }
        return transports
    }
}