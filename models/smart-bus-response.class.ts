import { SmartBusResponse, STATUS } from "./smart-bus.response";

export class SmartBusResponseClass<T> implements SmartBusResponse<T>{
    status: STATUS;    
    message: string | Object;
    data?: T ;


}