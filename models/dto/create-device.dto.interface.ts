import { DEVICE_TYPE } from '@lib/models/interfaces/device.interface';

export interface CreateDeviceIneterface  {

    deviceId: string;
    type: DEVICE_TYPE;
}