import { RandomDeviceHelper } from './random-device.helper';
import * as faker from "faker"
import { Tuteur } from '@lib/models/interfaces/tuteur.inteface';
export class RandomTuteurHelper {

    static getTuteursByNbr(nbr: number) {
        let tuteurs: Tuteur[] = []
        for (let index = 0; index < nbr; index++) {
            let tuteur: Tuteur = {
                devices: RandomDeviceHelper.getRandomDeviceByNbr(faker.random.number({ min: 0, max: 3 })),
                email: faker.internet.email(),
                etat:faker.random.number({ min: 0, max: 3 })>2?-1:0,
                id: faker.random.number(),
                nom: faker.name.lastName(),
                prenom: faker.name.firstName(),
                tel: faker.phone.phoneNumber("+212"),
                userName: faker.random.words(2)
            }
            tuteurs.push(tuteur)
        }
        return tuteurs
    }
}