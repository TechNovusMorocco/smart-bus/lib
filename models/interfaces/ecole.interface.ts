import { Adresse } from "./adresse.interface";

export interface Ecole {
    id: number
    nomEcole: string
    email: string
    userName: string
    password: string
    tel: string
    /**
     * état de l'école .
     * 
     * Valeurs :
     *  - -2 supprimer 
     *  - -1 vient d'étre creer  
  */
    etat: number
    adresse: Adresse

}


