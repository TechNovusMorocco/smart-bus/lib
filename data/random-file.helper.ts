import { file, FILETYPE } from '@lib/models/interfaces/file.interface';
import * as Faker from "faker"
export class RandomFileHelper {


    public static getFilesByNbr(nbr: number) {
       let files:file[]=[]
        for (let i = 0; i < nbr; i++) {
            let file:file={
                creationDate:Faker.date.recent(30),
                id:Faker.random.number(100),
                type:this.randomFileType(),
                url:"https://i.picsum.photos/id/"+Faker.random.number({min:500,max:600})+"/900/500.jpg"
            }
            files.push(file)
        }

        return files
    }

    private static randomFileType(){
        let randomNumber=Faker.random.number(25)
        if(randomNumber<=5)
        return FILETYPE.EXCEL
        else if(randomNumber<=10)
        return FILETYPE.JPEG
        else if(randomNumber<=15)
        return FILETYPE.JPG
        else if (randomNumber <= 20)
        return FILETYPE.PDF
        else  return FILETYPE.PNG
    }
}