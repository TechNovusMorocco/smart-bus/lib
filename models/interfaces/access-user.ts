import { Classe } from './classe.interface';
import { Ecole } from './ecole.interface';
import { AccessRole } from './access-role';

export interface AccessUser {
    id:number
    userName: string
    email: string
    password: string
    tel: string
    roles: AccessRole[]
    firstName: string
    LastName: string
    /**
     * classes separated by ","
     */
    userClasses:string
    classes: string[]
    ecole: Ecole
}

