import { Ecole } from "./ecole.interface";
import { Conducteur } from "./conducteur.interface";
import { Accompagnatrice } from "./accompagnatrice.interface";

export interface Transport{
    id:number 
    /**
     * etat du transport .
     * 
     * values :
     *  - -2 supprimer 
     *  - -1 vient d'étre creer 
     *  
     */
    etat:number
    mattricule:string
    simNumber:string
    capacite:number
    ecole:Ecole
    conducteur:Conducteur
    accompagnatrice:Accompagnatrice
    createdAt:number
}